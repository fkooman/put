<?php

/*
 * Copyright (c) 2019-2022 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace PHPUnit\Framework;

use Exception;
use fkooman\Put\Exception\AssertionException;
use fkooman\Put\Exception\TestException;
use ReflectionClass;

class TestCase
{
    /** @var int */
    private $testCount = 0;

    /** @var int */
    private $riskyCount = 0;

    /** @var int */
    private $skippedCount = 0;

    /** @var int */
    private $assertionCount = 0;

    /** @var int */
    private $errorCount = 0;

    /** @var array<TestException> */
    private $errorList = [];

    /** @var string|null */
    private $expectedException = null;

    /** @var string|null */
    private $expectedExceptionMessage = null;

    /** @var int|null */
    private $expectedExceptionCode = null;

    /**
     * @param string $expected
     *
     * @return void
     */
    protected function expectException($expected)
    {
        ++$this->assertionCount;
        $this->expectedException = $expected;
    }

    /**
     * @param string $expectedExceptionMessage
     *
     * @return void
     */
    protected function expectExceptionMessage($expectedExceptionMessage)
    {
        ++$this->assertionCount;
        $this->expectedExceptionMessage = $expectedExceptionMessage;
    }

    /**
     * @param int $expectedExceptionCode
     *
     * @return void
     */
    protected function expectExceptionCode($expectedExceptionCode)
    {
        ++$this->assertionCount;
        $this->expectedExceptionCode = $expectedExceptionCode;
    }

    /**
     * @return void
     */
    protected function setUp() {}

    /**
     * @param int $i
     * @return void
     */
    protected function addToAssertionCount($i)
    {
        $this->assertionCount += $i;
    }

    /**
     * @param mixed $condition
     *
     * @return void
     */
    protected function assertTrue($condition)
    {
        ++$this->assertionCount;
        if (true !== $condition) {
            throw new AssertionException('true', $condition);
        }
    }

    /**
     * @param mixed $condition
     *
     * @return void
     */
    protected function assertFalse($condition)
    {
        ++$this->assertionCount;
        if (false !== $condition) {
            throw new AssertionException('false', $condition);
        }
    }

    /**
     * @param mixed $expected
     * @param mixed $actual
     *
     * @return void
     */
    protected function assertInstanceOf($expected, $actual)
    {
        ++$this->assertionCount;
        if (!($actual instanceof $expected)) {
            throw new AssertionException($expected, $actual);
        }
    }

    /**
     * @param mixed $variable
     *
     * @return void
     */
    protected function assertNull($variable)
    {
        ++$this->assertionCount;
        if (null !== $variable) {
            throw new AssertionException('null', $variable);
        }
    }

    /**
     * @param mixed $variable
     *
     * @return void
     */
    protected function assertNotNull($variable)
    {
        ++$this->assertionCount;
        if (null === $variable) {
            throw new AssertionException('!null', $variable);
        }
    }

    /**
     * @param mixed $expected
     * @param mixed $actual
     *
     * @return void
     */
    protected function assertEquals($expected, $actual)
    {
        ++$this->assertionCount;
        if ($expected != $actual) {
            throw new AssertionException($expected, $actual);
        }
    }

    /**
     * @param mixed $expected
     * @param mixed $actual
     *
     * @return void
     */
    protected function assertNotEquals($expected, $actual)
    {
        ++$this->assertionCount;
        if ($expected == $actual) {
            throw new AssertionException($expected, $actual);
        }
    }

    /**
     * @param string $needle
     * @param string $haystack
     *
     * @return void
     */
    protected function assertStringContainsString($needle, $haystack)
    {
        ++$this->assertionCount;
        if (false === strpos($haystack, $needle)) {
            throw new AssertionException($needle, $haystack);
        }
    }

    /**
     * @param string $needle
     * @param string $haystack
     *
     * @return void
     */
    protected function assertStringStartsWith($needle, $haystack)
    {
        ++$this->assertionCount;
        if (0 !== strpos($haystack, $needle)) {
            throw new AssertionException($needle, $haystack);
        }
    }

    /**
     * @param mixed $key
     *
     * @return void
     */
    protected function assertArrayHasKey($key, array $array)
    {
        ++$this->assertionCount;
        if (!array_key_exists($key, $array)) {
            throw new AssertionException($key, $array);
        }
    }

    /**
     * @param mixed $needle
     *
     * @return void
     */
    protected function assertContains($needle, array $haystack)
    {
        ++$this->assertionCount;
        if (!in_array($needle, $haystack, true)) {
            throw new AssertionException($needle, $haystack);
        }
    }

    /**
     * @param mixed $needle
     *
     * @return void
     */
    protected function assertNotContains($needle, array $haystack)
    {
        ++$this->assertionCount;
        if (in_array($needle, $haystack, true)) {
            throw new AssertionException($needle, $haystack);
        }
    }

    /**
     * @param string $pattern
     * @param mixed  $actual
     *
     * @return void
     */
    protected function assertMatchesRegularExpression($pattern, $actual)
    {
        self::assertRegExp($pattern, $actual);
    }

    /**
     * @param string $pattern
     * @param mixed  $actual
     *
     * @return void
     */
    protected function assertRegExp($pattern, $actual)
    {
        if (1 !== preg_match($pattern, $actual)) {
            throw new AssertionException($pattern, $actual);
        }
    }

    /**
     * @param mixed $expected
     * @param mixed $actual
     *
     * @return void
     */
    protected function assertSame($expected, $actual)
    {
        ++$this->assertionCount;
        if ($expected !== $actual) {
            throw new AssertionException($expected, $actual);
        }
    }

    /**
     * @param mixed $expected
     * @param mixed $actual
     *
     * @return void
     */
    protected function assertNotSame($expected, $actual)
    {
        ++$this->assertionCount;
        if ($expected === $actual) {
            throw new AssertionException($expected, $actual);
        }
    }

    /**
     * @param mixed $variable
     *
     * @return void
     */
    protected function assertEmpty($variable)
    {
        ++$this->assertionCount;
        if (!empty($variable)) {
            throw new AssertionException('empty', $variable);
        }
    }

    /**
     * @param mixed $variable
     *
     * @return void
     */
    protected function assertNotEmpty($variable)
    {
        ++$this->assertionCount;
        if (empty($variable)) {
            throw new AssertionException('!empty', $variable);
        }
    }

    /**
     * @param mixed $expected
     * @param mixed $actual
     *
     * @return void
     */
    protected function assertGreaterThan($expected, $actual)
    {
        // PHPUnit seems to count "or" assertions as two assertions...
        $this->assertionCount++;
        if ($actual <= $expected) {
            throw new AssertionException($expected, $actual);
        }
    }

    /**
     * @param mixed $expected
     * @param mixed $actual
     *
     * @return void
     */
    protected function assertGreaterThanOrEqual($expected, $actual)
    {
        // PHPUnit seems to count "or" assertions as two assertions...
        $this->assertionCount += 2;
        if ($actual < $expected) {
            throw new AssertionException($expected, $actual);
        }
    }

    /**
     * @param string $expected
     * @param mixed  $actual
     *
     * @return void
     */
    protected function assertInternalType($expected, $actual)
    {
        ++$this->assertionCount;
        if ($expected !== gettype($actual)) {
            throw new AssertionException($expected, $actual);
        }
    }

    /**
     * @param int   $expected
     * @param mixed $actual
     *
     * @return void
     */
    protected function assertCount($expected, $actual)
    {
        ++$this->assertionCount;
        if (!is_countable($actual)) {
            throw new AssertionException($expected, $actual);
        }
        if ($expected !== count($actual)) {
            throw new AssertionException($expected, $actual);
        }
    }

    /**
     * @return void
     */
    protected function fail()
    {
        ++$this->assertionCount;
        throw new TestException('fail');
    }

    /**
     * @return void
     */
    protected function markTestSkipped()
    {
        ++$this->skippedCount;
    }

    /**
     * @return int
     */
    public function noOfAssertions()
    {
        return $this->assertionCount;
    }

    /**
     * @return int
     */
    public function noOfTests()
    {
        return $this->testCount;
    }

    /**
     * @return int
     */
    public function noOfRiskyTests()
    {
        return $this->riskyCount;
    }

    /**
     * @return int
     */
    public function noOfSkippedTests()
    {
        return $this->skippedCount;
    }

    /**
     * @return int
     */
    public function noOfErrors()
    {
        return $this->errorCount;
    }

    /**
     * @return array<TestException>
     */
    public function errorList()
    {
        return $this->errorList;
    }

    /**
     * @param string $classMethod
     * @param ?array $dpd
     * @return void
     */
    private function runTestMethod($classMethod, array $dpd = null)
    {
        $expectedException = null;
        $expectedExceptionMessage = null;
        $expectedExceptionCode = null;
        $this->expectedException = null;
        $this->expectedExceptionMessage = null;
        $this->expectedExceptionCode = null;

        // if "setUp" method is there, always run it before the test method
        if (method_exists($this, 'setUp')) {
            $this->setUp();
        }

        $preAssertionCount = $this->assertionCount;
        $preSkippedCount = $this->skippedCount;
        try {
            ++$this->testCount;
            if (null === $dpd) {
                $this->$classMethod();
            } else {
                call_user_func_array([$this, $classMethod], $dpd);
            }

            // did we expect an exception but didn't get one?
            $expectedException = $this->expectedException;
            if (null !== $expectedException) {
                throw new TestException(sprintf('no exception "%s" thrown in "%s"', $expectedException, $classMethod));
            }
        } catch (Exception $e) {
            // did we expect one?!
            $expectedException = $this->expectedException;
            $expectedExceptionMessage = $this->expectedExceptionMessage;
            $expectedExceptionCode = $this->expectedExceptionCode;
            if (null === $expectedException) {
                throw new TestException(sprintf('unexpected exception "%s" thrown in "%s": %s', get_class($e), $classMethod, $e->getMessage()));
            }

            if (get_class($e) !== $expectedException) {
                // the expected exception MAY also be a parent of $e
                if (!self::isParent($e, $expectedException)) {
                    throw new TestException(sprintf('exception "%s" thrown, expected type "%s" in "%s"', get_class($e), $expectedException, $classMethod));
                }
            }
            if (null !== $expectedExceptionMessage) {
                if ($expectedExceptionMessage !== $e->getMessage()) {
                    throw new TestException(sprintf('exception message is "%s", expected "%s"', $e->getMessage(), $expectedExceptionMessage));
                }
            }
            if (null !== $expectedExceptionCode) {
                if ($expectedExceptionCode !== $e->getCode()) {
                    throw new TestException(sprintf('exception code is "%d", expected "%d"', $e->getCode(), $expectedExceptionCode));
                }
            }
        }
        $postAssertionCount = $this->assertionCount;
        $postSkippedCount = $this->skippedCount;
        // XXX do not count skipped tests as risky!
        if ($preSkippedCount !== $postSkippedCount) {
            echo 'S';
        } elseif ($preAssertionCount === $postAssertionCount) {
            echo 'R';
            ++$this->riskyCount;
        } else {
            echo '.';
        }
    }

    /**
     * @return void
     */
    public function run()
    {
        $classMethods = get_class_methods($this);
        // find all methods with a name that start with test and call them
        foreach ($classMethods as $classMethod) {
            try {
                if (0 === strpos($classMethod, 'test')) {
                    $hasDataProvider = false;
                    // look at @dataProvider stuff
                    $rc = new ReflectionClass(get_class($this));
                    if (false !== $docComment = $rc->getMethod($classMethod)->getDocComment()) {
                        // we got docComment
                        //extract "dataProvider"
                        //call classmethod with dataProvider stuff
                        if (1 === preg_match('/@dataProvider (.*)/', $docComment, $matches)) {
                            $hasDataProvider = true;
                            $dataProviderMethod = $matches[1];
                            // call this method and loop over the array response to call our
                            // test method
                            $dataProviderData = $this->$dataProviderMethod();
                            foreach ($dataProviderData as $dpd) {
                                // every dateProvider result is one test
                                $this->runTestMethod($classMethod, $dpd);
                            }
                        }
                    }

                    if (!$hasDataProvider) {
                        // no @dataProvider
                        $this->runTestMethod($classMethod);
                    }
                }
            } catch (TestException $e) {
                echo 'E';
                ++$this->errorCount;
                $this->errorList[] = $e;
            }
        }
    }

    /**
     * @param string $expectedException
     * @return bool
     */
    private static function isParent(Exception $thrownException, $expectedException)
    {
        while (false !== $parentClass = get_parent_class($thrownException)) {
            if ($parentClass === $expectedException) {
                return true;
            }
            $thrownException = $parentClass;
        }

        return false;
    }
}
