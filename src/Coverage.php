<?php

/*
 * Copyright (c) 2019-2022 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\Put;

use RuntimeException;

class Coverage
{
    /**
     * @param string $outputFile
     * @param string $currentWorkingDir
     *
     * @return void
     */
    public static function writeReport($outputFile, $currentWorkingDir, array $coverageData)
    {
        // get the source code from the files analyzed by the code coverage
        $templateData = [];

        // add all files to coverageData without coverage
        $fileList = self::listDir($currentWorkingDir . '/src');
        foreach ($fileList as $fileEntry) {
            if (!array_key_exists($fileEntry, $coverageData)) {
                $coverageData[realpath($fileEntry)] = [
                    'coveragePercent' => 0,
                    'coverageData' => [],
                ];
            }
        }

        $sourceFileList = array_keys($coverageData);
        foreach ($sourceFileList as $srcFile) {
            if (false === $filePointer = @fopen($srcFile, 'r')) {
                throw new RuntimeException(sprintf('unable to open "%s"', $srcFile));
            }
            $srcCode = [];
            while (false !== $srcLine = fgets($filePointer)) {
                $srcCode[] = htmlentities(rtrim($srcLine));
            }

            $srcName = substr($srcFile, strlen($currentWorkingDir) + 1);
            $templateData[$srcName] = [
                'srcCode' => $srcCode,
                'coverageData' => $coverageData[$srcFile],
                'coveragePercent' => self::calculatePercentage($coverageData[$srcFile]),
            ];
        }

        // sort the $templateData array by coveragePercent
        uasort($templateData, function ($a, $b) {
            if ($a['coveragePercent'] == $b['coveragePercent']) {
                return 0;
            }

            return ($a['coveragePercent'] < $b['coveragePercent']) ? -1 : 1;
        });

        ob_start();
        include __DIR__ . '/tpl/Coverage.php';
        $htmlPage = ob_get_contents();
        ob_end_clean();
        if (false === @file_put_contents($outputFile, $htmlPage)) {
            throw new RuntimeException(sprintf('unable to write to "%s"', $outputFile));
        }
    }

    /**
     * @return int
     */
    private static function calculatePercentage(array $coverageData)
    {
        $coveredAmount = 0;
        $uncoveredAmount = 0;
        foreach ($coverageData as $rowCovered) {
            if (1 === $rowCovered) {
                ++$coveredAmount;
            } else {
                ++$uncoveredAmount;
            }
        }

        return (int) floor($coveredAmount / ($coveredAmount + $uncoveredAmount) * 100);
    }

    /**
     * @param string $d
     *
     * @return array<string>
     */
    private static function listDir($d)
    {
        $x = [];
        foreach (glob($d . '/*', GLOB_ERR) as $f) {
            if (!is_dir($f)) {
                $x[] = $f;
                continue;
            }
            $x = array_merge($x, self::listDir($f));
        }

        return $x;
    }
}
