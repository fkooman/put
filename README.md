# PHP Unit Testing for Minimalists

A simple PHPUnit replacement with support for PHP >= 5.4. It implements the
most common PHPUnit assertions. In addition, it supports PHP Code Coverage
using [pcov](https://github.com/krakjoe/pcov) on PHP >= 7. For `pcov` to work,
make sure you enable it.

**NOTE**: this is NOT meant as a (full) replacement for `phpunit/phpunit`! It 
is a barebones PHPUnit clone that you can use to run your unit tests in 
minimalist environments when you don't need the full feature set of PHPUnit.

We support a few common assertions, and the recommended way to test exceptions, 
nothing more. No mocks, and no PHPUnit annotations. Those should not be used 
anyway as that reduces the effectiveness of static code analysis.

## Assertions

The following assertions are implemented, more can be easily added:

* `addToAssertionCount()`
* `assertArrayHasKey()`
* `assertContains()`
* `assertCount()`
* `assertEmpty()`
* `assertEquals()`
* `assertFalse()`
* `assertGreaterThan()`
* `assertGreaterThanOrEqual()`
* `assertInstanceOf()`
* `assertInternalType()`
* `assertNotContains()`
* `assertNotEmpty()`
* `assertNotEquals()`
* `assertNotNull()`
* `assertNotSame()`
* `assertNull()`
* `assertRegexp()`
* `assertMatchesRegularExpression()`
* `assertSame()`
* `assertStringContainsString()`
* `assertStringStartsWith()`
* `assertTrue()`
* `fail()`

## Exceptions

For testing exceptions we implemented:

* `expectException()`
* `expectExceptionMessage()`
* `expectExceptionCode()`

For example:

```php
$this->expectException('RangeException');
```

Or when using PHP >= 5.5:

```php
$this->expectException(RangeException::class);
```

## Using

This project requires PHP >= 5.4. It has no other dependencies, but optionally
`ext-pcov` for code coverage reporting.

In your project's `composer.json`:

```json
"repositories": [
    {
        "type": "vcs",
        "url": "https://codeberg.org/fkooman/put"
    }
],

...

"require-dev": {
    "fkooman/put": "^0"
},
```

Do not forget to run `composer update`.

## Writing Tests

Writing tests is exactly the same as for PHPUnit. Put them in the `tests/` 
directory of your project. A simple example:

```php
<?php

namespace my\app;

use DateTime;
use PHPUnit\Framework\TestCase;

class SimpleTest extends TestCase
{
    public function testDate()
    {
        $dateTime = new DateTime('2019-01-01 08:00:00');
        $this->assertSame('2019-01-01', $dateTime->format('Y-m-d'));
    }
}
```

This makes it easy to support both `phpunit` and `put` with the same test 
suite.

## Running Tests

Assuming you added `put` to your `composer.json`, you can simply run it:

```bash
$ vendor/bin/put
.............
#Tests      : 13
#Assertions : 14
$ echo $?
0
```

See `vendor/bin/put --help` for a description of the configuration options. 

As an example, with the defaults:

```bash
$ vendor/bin/put --bootstrap vendor/autoload.php --suffix Test.php tests
```

To run code coverage reporting, using the defaults:

```bash
$ vendor/bin/put --coverage report.html
```

You can view the `report.html` in your web browser. If this does not work, 
make sure you have the `pcov` extension installed and enabled, and 
`pcov.enabled` set to `1`.

### Test Failure

```bash
$ vendor/bin/put
....E.
#Tests      : 6
#Assertions : 6
#Errors     : 1
**** ERROR ****
[fkooman\Put\Exception\AssertSameException]
--- EXPECTED ---
'2019-01-02'
--- ACTUAL ---
'2019-01-01'
--- END ---
#0 /home/fkooman/Projects/put/tests/SimpleTest.php(16): PHPUnit\Framework\TestCase->assertSame('2019-01-02', '2019-01-01')
#1 /usr/share/php/fkooman/Put/TestCase.php(288): fkooman\Put\SimpleTest->testDate()
#2 /usr/share/php/fkooman/Put/Put.php(85): PHPUnit\Framework\TestCase->run()
#3 /usr/bin/put(22): fkooman\Put\Put->run(Array, '/home/fkooman/P...')
#4 {main}

$ echo $?
1
```

From the trace your can determine where it went wrong. The output is not as 
sophisticated as the PHPUnit output, but hey!
