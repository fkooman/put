.PHONY: all test fmt psalm phpstan sloc

all:	test fmt psalm phpstan

test:
	bin/put

fmt:
	php-cs-fixer fix

psalm:
	psalm

phpstan:
	phpstan

sloc:
	phploc bin src
