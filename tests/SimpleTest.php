<?php

/*
 * Copyright (c) 2019-2022 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\Put;

use DateTime;
use PHPUnit\Framework\TestCase;

class SimpleTest extends TestCase
{
    /**
     * @return void
     */
    public function testDate()
    {
        $dateTime = new DateTime('2019-01-01 08:00:00');
        $this->assertSame('2019-01-01', $dateTime->format('Y-m-d'));
    }

    /**
     * @return void
     */
    public function testAssertEquals()
    {
        $this->assertEquals(5, '5');
    }

    /**
     * @return void
     */
    public function testAssertNotEquals()
    {
        $this->assertNotEquals(4, '5');
    }

    /**
     * @return void
     */
    public function testAssertEmpty()
    {
        $this->assertEmpty([]);
    }

    /**
     * @return void
     */
    public function testAssertStringContainsString()
    {
        $this->assertStringContainsString('foo', 'foobar');
    }

    /**
     * @return void
     */
    public function testAssertArrayHasKey()
    {
        $this->assertArrayHasKey('foo', ['foo' => 'bar']);
    }

    /**
     * @return void
     */
    public function testAssertContains()
    {
        $this->assertContains('foo', ['foo', 'bar']);
    }

    /**
     * @return void
     */
    public function testAssertGreaterThan()
    {
        $this->assertGreaterThan(4, 5);
    }

    /**
     * @return void
     */
    public function testNotAssertGreaterThan()
    {
        $this->expectException('fkooman\Put\Exception\AssertionException');
        $this->expectExceptionMessage(
            <<< 'EOF'
--- EXPECTED ---
4
--- ACTUAL ---
4
--- END ---
EOF
        );
        $this->assertGreaterThan(4, 4);
    }

    /**
     * @return void
     */
    public function testAssertGreaterThanOrEqual()
    {
        $this->assertGreaterThanOrEqual(4, 4);
    }

    /**
     * @return void
     */
    public function testNotAssertGreaterThanOrEqual()
    {
        $this->expectException('fkooman\Put\Exception\AssertionException');
        $this->expectExceptionMessage(
            <<< 'EOF'
--- EXPECTED ---
4
--- ACTUAL ---
3
--- END ---
EOF
        );
        $this->assertGreaterThanOrEqual(4, 3);
    }
}
