# ChangeLog

## 0.3.5 (2024-01-15)
- add `TestCase::assertMatchesRegularExpression`
- add some additional tests
- add `TestCase::assertGreaterThan`
- source formatting

## 0.3.4 (2023-04-11)
- add `addToAssertionCount()`

## 0.3.3 (2022-11-07)
- add Makefile
- update (C), php-cs-fixer file
- Throwable -> Exception

## 0.3.2 (2022-10-27)
- fixes for PHP 5.4

## 0.3.1 (2022-10-16)
- always set `date.timezone` to UTC

## 0.3.0 (2022-06-16)
- match assertion count with PHPUnit with `assertGreaterThanOrEqual`
- also count and run tests for expected exceptions with dataProvider, also 
  accept parent exception classes
- add test with dataProvider and exceptions

## 0.2.0 (2022-05-25)
- implement support for 
  [Data Providers](https://phpunit.readthedocs.io/en/stable/writing-tests-for-phpunit.html#data-providers)
- Add assertions:
  - `assertNotContains()`
  - `assertRegexp()`
  - `assertStringStartsWith()`
- list all source files in coverage report
- also count `expectExceptionMessage()` and `expectExceptionCode()` as 
  assertion
-  make it possible to test just one file

## 0.1.1 (2021-12-07)
- Add assertions:
  - `assertArrayHasKey()`
  - `assertContains()`
  - `assertEmpty()`
  - `assertNotEquals()`
  - `assertStringContainsString()`
- Add `expectExceptionCode()`

## 0.1.0 (2021-12-07)
- initial release
